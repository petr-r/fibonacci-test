﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace FibonacciTest.Models
{
    public class TestResponse
    {
        [JsonProperty("result")]
        [JsonConverter(typeof(StringEnumConverter))]
        public TestResult Result { get; set; }
    }
}