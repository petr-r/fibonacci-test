﻿using System;
using Newtonsoft.Json;

namespace FibonacciTest.Models
{
    public class TestRequest
    {
        [JsonProperty("url")]
        public string Url { get; set; }
    }
}