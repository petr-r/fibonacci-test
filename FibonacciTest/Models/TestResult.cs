﻿using System;

namespace FibonacciTest.Models
{
    public enum TestResult
    {
        None = 0,
        Pass = 1,
        Fail = 2
    }
}