﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using FibonacciTest.Models;

namespace FibonacciTest.Controllers
{
    public class TestController : ApiController
    {
        [Route("test")]
        public async Task<IHttpActionResult> Post(TestRequest request)
        {
            var client = new HttpClient();

            var n = new[] {1, 2, 3, 4, 5, 6, 46};
            var fib = new[] {1, 1, 2, 3, 5, 8, 1836311903};

            for (var i = 0; i < n.Length; i++)
            {
                if (!await TestAsync(request, client, n[i], fib[i]))
                {
                    return Json(new TestResponse
                    {
                        Result = TestResult.Fail
                    });
                }

            }

            return Json(new TestResponse
            {
                Result = TestResult.Pass
            });
        }

        private async Task<bool> TestAsync(TestRequest request, HttpClient client, int n, int fib)
        {
            try
            {
                var response = await client.GetAsync($"{request.Url}/fibonacci/{n}");
                if (response?.StatusCode == HttpStatusCode.OK)
                {
                    if (await response.Content?.ReadAsStringAsync() == fib.ToString())
                    {
                        return true;
                    }
                }
            }
            catch
            {
            }

            return false;
        }
    }
}